@component('mail::message')
    # Hello, {{ $user->name }}

    Thankyou for creating an account with us. Please Verify your account using the following link. If you have any problem
    contact us on "admin@somedomain.com"

    @component('mail::button', ['url' => route('users.verify', $user->verification_token)])
        Verify Account
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
