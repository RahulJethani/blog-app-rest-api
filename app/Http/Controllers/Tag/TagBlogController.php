<?php


namespace App\Http\Controllers\Tag;


use App\Models\Tag;

class TagBlogController extends \App\Http\Controllers\ApiController
{
    public function index(Tag $tag)
    {
        $blogs = $tag->blogs;
        return $this->showAll($blogs);
    }
}
