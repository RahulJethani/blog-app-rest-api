<?php


namespace App\Http\Controllers\Tag;


use App\Http\Controllers\ApiController;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagsController extends ApiController
{
    public function index()
    {
        return $this->showAll(Tag::all());
    }

    public function show(Tag $tag)
    {
        return $this->showOne($tag);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:2',
        ];

        $this->validate($request, $rules);

        $tag = Tag::create($request->only('name'));

        return $this->showOne($tag);
    }

    public function update(Request $request, Tag $tag)
    {
        $tag->fill($request->only(['name']));

        if( $tag->isClean()) {
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $tag->save();
        return $this->showOne($tag);
    }

    public function destroy(Tag $tag)
    {
        $tag->delete();
        return $this->showOne($tag);
    }
}
