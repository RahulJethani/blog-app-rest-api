<?php


namespace App\Http\Controllers\Blog;


use App\Models\Blog;

class BlogTagController extends \App\Http\Controllers\ApiController
{
    public function index(Blog $blog)
    {
        return $this->showAll($blog->tags);
    }
}
