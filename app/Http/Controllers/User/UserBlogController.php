<?php

namespace App\Http\Controllers\User;

use App\Models\Blog;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserBlogController extends \App\Http\Controllers\ApiController
{
    public function index(User $user)
    {
        $blogs =   $user->blogs;
        return $this->showAll($blogs);
    }

    public function store(Request $request, User $user)
    {
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image',
            'category_id'=> 'exists:categories,id',
            'tags.*' => 'exists:tags,id'
        ];

        $this->validate($request, $rules);
        $data = $request->all();

        $data['image'] = $request->image->store('blogs');
        $data['user_id'] = $user->id;

        $blog = Blog::create($data);
        $blog->tags()->attach($request->tags);

        return $this->showOne($blog);
    }

    public function update(Request $request, User $user, Blog $blog)
    {
        $this->verifyUser($user, $blog);

        $rules = [
            'name' => 'min:2',
            'description' => 'min:5',
            'image' => 'image',
            'category_id'=>'exists:categories,id'
        ];

        $this->validate($request, $rules);

        $blog->fill(
            $request->only([
                'name',
                'description',
                'category_id'
            ])
        );

        if($request->hasFile('image')) {
            Storage::delete($blog->image);
            $blog->image = $request->image->store('blogs');
        }

        if($blog->isClean()) {
            return $this->errorResponse("You have not updated any value!!", 422);
        }

        if($request->has('tags')){
            $this->validate($request, [
                'tags.*' => 'exists:tags,id'
            ]);
            $blog->tags()->sync($request->tags);
        }

        $blog->save();
        return $this->showOne($blog);
    }

    public function destroy(User $user, Blog $blog)
    {
        $this->verifyUser($user, $blog);
        Storage::delete($blog->image);
        $blog->tags()->detach($blog->tags);
        $blog->delete();
        return $this->showOne($blog);
    }

    private function verifyUser(User $user, Blog $blog)
    {
        if($user->id != $blog->user_id) {
            throw new HttpException(422, "You are trying to update someone else's blog!");
        }
    }
}
