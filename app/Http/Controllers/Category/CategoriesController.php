<?php


namespace App\Http\Controllers\Category;


use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends \App\Http\Controllers\ApiController
{
    public function index()
    {
        return $this->showAll(Category::all());
    }

    public function show(Category $category)
    {
        return $this->showOne($category);
    }


    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:2',
            'description' => 'required|min:2'
        ];

        $this->validate($request, $rules);

        $category = Category::create($request->only('name', 'description'));

        return $this->showOne($category);
    }

    public function update(Request $request, Category $category)
    {
        $category->fill($request->only(['name', 'description']));

        if( $category->isClean()) {
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $category->save();
        return $this->showOne($category);
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return $this->showOne($category);
    }
}
