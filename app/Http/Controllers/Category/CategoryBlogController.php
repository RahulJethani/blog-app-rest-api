<?php


namespace App\Http\Controllers\Category;


use App\Http\Controllers\ApiController;
use App\Models\Category;

class CategoryBlogController extends ApiController
{
    public function index(Category $category)
    {
        $blogs = $category->blogs;
        return $this->showAll($blogs);
    }
}
