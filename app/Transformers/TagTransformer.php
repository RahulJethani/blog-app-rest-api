<?php

namespace App\Transformers;

use App\Models\Tag;
use League\Fractal\TransformerAbstract;

class TagTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Tag $tag)
    {
        return [
            'identifier' => (int)$tag->id,
            'title' => $tag->name,
            'creationDate' => $tag->created_at,
            'lastChangeDate' => $tag->updated_at,
            'deletionDate' => $tag->deleted_at ?? null,

            /* HATEOAS IMPLEMENTATION */
            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('tags.show', $tag->id)
                ],
                [
                    'rel' => 'tags.blogs',
                    'href' => route('tags.blogs.index', $tag
                        ->id)
                ],
            ],
        ];

    }

    public static function getOriginalAttribute(string $transformedAttribute)
    {
        $attribute = [
            'identifier' => 'id',
            'title' => 'name',
            'creationDate' => 'created_at',
            'lastChangeDate' => 'updated_at',
            'deletionDate' => 'deleted_at',
        ];

        return $attribute[$transformedAttribute] ?? null;
    }
}
