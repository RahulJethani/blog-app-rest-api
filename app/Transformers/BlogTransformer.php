<?php

namespace App\Transformers;

use App\Models\Blog;
use League\Fractal\TransformerAbstract;

class BlogTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Blog $blog)
    {
        return [
            'id' => (int)$blog->id,
            'title' => $blog->name,
            'description' => $blog->description,
            'date' => $blog->created_at,
            'views' => $blog->views,
            'rating' => $blog->rating,
            'image' => url("images/{$blog->image}"),
            'author' => $blog->user_id,
            'category' => $blog->category_id,
            'lastChangeDate' => $blog->updated_at,
            'deletionDate' => $blog->deleted_at ?? null,

            /* HATEOAS IMPLEMENTATION */
            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('blogs.show', $blog->id)
                ],
                [
                    'rel' => 'category',
                    'href' => route('categories.show', $blog->category_id)
                ],
                [
                    'rel' => 'blog.tags',
                    'href' => route('blogs.tags.index', $blog->id)
                ],
                [
                    'rel' => 'author',
                    'href' => route('users.show', $blog->user_id)
                ],
            ],
        ];
    }

    public static function getOriginalAttribute(string $transformedAttribute)
    {
        $attribute = [
            'id' => 'id',
            'title' => 'name',
            'description' => 'description',
            'views' => 'views',
            'image' => 'image',
            'rating' => 'rating',
            'date' => 'created_at',
            'category_id' => 'category',
            'user_id' => 'author',
            'lastChangeDate' => 'updated_at',
            'deletionDate' => 'deleted_at',
        ];

        return $attribute[$transformedAttribute] ?? null;
    }
}
