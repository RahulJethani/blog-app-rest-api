<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'identifier' => (int)$user->id,
            'name' => $user->name,
            'email' => $user->email,
            'avatar' => url("images/{$user->image}"),
            'isVerified' => (bool) $user->isVerified(),
            'creationDate' => $user->created_at,
            'lastChangeDate' => $user->updated_at,
            'deletionDate' => $user->deleted_at ?? null,

            /* HATEOAS IMPLEMENTATION */
            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('users.show', $user->id)
                ],
                [
                    'rel' => 'users.blogs',
                    'href' => route('users.blogs.index', $user->id)
                ],
            ],
        ];
    }

    public static function getOriginalAttribute(string $transformedAttribute)
    {
        $attribute = [
            'identifier' => 'id',
            'name' => 'name',
            'email' => 'email',
            'isVerified' => 'verified',
            'avatar' => 'image',
            'creationDate' => 'created_at',
            'lastChangeDate' => 'updated_at',
            'deletionDate' => 'deleted_at',
        ];

        return $attribute[$transformedAttribute] ?? null;
    }
}
