<?php

namespace App\Models;

use App\Transformers\CategoryTransformer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;

    public string $transformer = CategoryTransformer::class;

    protected $fillable = [
        'name',
        'description'
    ];

    public function blogs(): HasMany
    {
        return $this->hasMany(Blog::class);
    }
}
