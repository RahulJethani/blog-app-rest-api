<?php

namespace Database\Seeders;

use App\Models\Blog;
use App\Models\Category;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::flushEventListeners();

        if(App::environment() === 'production'){
            exit();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $numOfUsers = 100;
        $numOfCategories = 20;
        $numOfTags = 10;
        $numOfBlogs = 400;


        User::factory()->count($numOfUsers)->create();
        Category::factory()->count($numOfCategories)->create();
        Tag::factory()->count($numOfTags)->create();

        Blog::factory()->count($numOfBlogs)->create()->each(function (Blog $blog){
            $tagIDs = Tag::all()->random(mt_rand(1, 5))->pluck('id');
            $blog->tags()->attach($tagIDs);
        });
    }
}
