<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::resource('blogs',\App\Http\Controllers\Blog\BlogsController::class)
    ->only('index', 'show');
Route::resource('blogs.tags', \App\Http\Controllers\Blog\BlogTagController::class)
    ->only('index');

Route::resource('categories', \App\Http\Controllers\Category\CategoriesController::class)
    ->except('create', 'edit');
Route::resource('categories.blogs', \App\Http\Controllers\Category\CategoryBlogController::class)
    ->only('index');

Route::resource('tags', \App\Http\Controllers\Tag\TagsController::class)
    ->except('create', 'edit');
Route::resource('tags.blogs', \App\Http\Controllers\Tag\TagBlogController::class)
    ->only('index');

Route::resource('users', \App\Http\Controllers\User\UsersController::class)
    ->except('create', 'edit');
Route::resource('users.blogs', \App\Http\Controllers\User\UserBlogController::class)
    ->except('create', 'edit');
Route::get('users/verify/{token}', [\App\Http\Controllers\User\UsersController::class, 'verify'])
    ->name('users.verify');
Route::get('users/{user}/resend-verification-email', [\App\Http\Controllers\User\UsersController::class, 'resend'])
    ->name('users.resend');
